#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>

#include <stdio.h>
#include <util/delay.h>

#define TRUE 1
#define FALSE 0

#define CKP_AVERAGE_ITEMS 8 // ! only power of two
#define CKP_AVERAGE_SHIFT 3 // ! shift for devide


struct _global {
	char *p_timerS; // timer for checking short events
	
	struct _pins {
		unsigned char CKP : 1;
		unsigned char debugPin : 1;
	} P;
	struct _flags {
		unsigned char isCKPImpulse : 1;
	} F;
	
	struct _CKP {
		unsigned char periodLenC;                    // current period length
		unsigned char periodLenAverage;              // average period length
		unsigned char periodLenUnion;                // for calculate the average period length
		unsigned char periodLenP[CKP_AVERAGE_ITEMS]; // history for calculate the average period length
		unsigned char averageIndex;                  // index for calculate the average period length
		unsigned char frontP;                        // privious impulse front timer value
		unsigned char periodLenPrev;
	} CKP;
	
} G;

void init() {
	DDRA=0x00; // set in
	DDRB=0xff; // set out
	
	PORTB = 0x00; // set zero volt
	PORTA = 0xff; // switch on pullup resistors
	
	TCCR0 = (1<<CS02)|(0<<CS01)|(1<<CS00); // set devider 1024
	TCNT0 = 0; // timer reset
	
	// binding
	G.p_timerS = &TCNT0;
}

void update() { // is no fast but is easy
	// *** in section ***
	G.P.CKP = PINA & (1<<0);
	
	// *** out section ***
	if (G.P.debugPin) PORTB |= (1<<0);
	else PORTB &= ~(1<<0);
}

void average(unsigned char val) {
	if (G.CKP.averageIndex < CKP_AVERAGE_ITEMS) G.CKP.averageIndex++;
	else G.CKP.averageIndex = 0;
	
	G.CKP.periodLenUnion -= G.CKP.periodLenP[G.CKP.averageIndex];       // remove old value
	G.CKP.periodLenP[G.CKP.averageIndex] = val;
	G.CKP.periodLenUnion += G.CKP.periodLenP[G.CKP.averageIndex];       // add new value
	
	G.CKP.periodLenAverage = G.CKP.periodLenUnion >> CKP_AVERAGE_SHIFT; // devide
}

void calcCKP() {
	if (G.P.CKP) { // is CKP impulse
		if (!G.F.isCKPImpulse) { // is CKP impulse upper front 
			G.CKP.periodLenC = *G.p_timerS - G.CKP.frontP;
			G.CKP.frontP = *G.p_timerS;
			G.F.isCKPImpulse = TRUE;

			//if (G.CKP.periodLenC >= (G.CKP.periodLenAverage << 1)) {
			if (G.CKP.periodLenC >= (G.CKP.periodLenPrev << 1)) {
				G.P.debugPin = TRUE; // zero impulse
			}
			//average(G.CKP.periodLenC);
			G.CKP.periodLenPrev = G.CKP.periodLenC;
		}
	} else {
		G.F.isCKPImpulse = FALSE; // for wait the CKP impulse upper front
		G.P.debugPin = FALSE;
	}
}

int main() {

	init();
	
	while(1) {       // main loop
		update();    // update all values
		calcCKP();   // calculate CKP values
	}
	return 0;
}
